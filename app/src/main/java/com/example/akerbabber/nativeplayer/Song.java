package com.example.akerbabber.nativeplayer;

/**
 * Created by akerbabber on 6/19/15.
 */
public class Song
{
    private long id;
    private String title;
    private String artist;
    public Song (long songID, String songTitle, String songArtist) {
        id=songID;
        title=songTitle;
        artist=songArtist;
    }
    public long getID(){
        return this.id;
    }
    public String getTitle() {
        return this.title;
    }
    public String getArtist() {
        return this.artist;
    }
}
